﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DBQueryLibraryEF1;
using DBQueryLibraryAdoNet;



namespace IShop_VS2017
{
    class Program
    {
        static void Main(string[] args)
        {
            if(DBQueryLibraryAdoNet.DBQuery.IsThereArticle("02.05.2019", 1, 2018))
                Console.WriteLine("There is enough TV Sets.");
            else
                Console.WriteLine("There is not enough TV Sets.");
            Console.WriteLine();
            Console.WriteLine("02.05.2019. TVSet cost " + DBQueryLibraryAdoNet.DBQuery.PriceToDate("02.05.2019", 1).ToString());
            Console.WriteLine();
            Console.WriteLine("order1 costs " + DBQueryLibraryAdoNet.DBQuery.TotalOrder(1).ToString()+" RUR.");
            Console.WriteLine();
            Console.WriteLine("Arrival of the 8 coffeemakers to the store ");
            DBQueryLibraryAdoNet.DBQuery.ToStore("21.12.2019", 3, 8);
            Console.WriteLine();
            Console.WriteLine(DBQueryLibraryAdoNet.DBQuery.StoreADay("01.12.2019"));
            Console.WriteLine();
            Console.WriteLine("Best client: " + DBQueryLibraryAdoNet.DBQuery.BestClient());
            Console.ReadKey();

        }
    }
}
