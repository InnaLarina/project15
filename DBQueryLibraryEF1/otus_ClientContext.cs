﻿using System.Data.Entity;


namespace DBQueryLibraryEF1
{
    class otus_ClientContext : DbContext
    {
        public otus_ClientContext() : base("DBConnection")
        { }
        public DbSet<otus_Client> otus_Clients { get; set; }
    }
}
