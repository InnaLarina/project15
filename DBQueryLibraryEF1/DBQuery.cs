﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    public static class DBQuery
    {
        public static void ToStore(string date1, int idArticle, int amount)
        {
            
            using (otus_StoreContext db = new otus_StoreContext())
            {
                // создаем объект otus_Store
                otus_Store store1 = new otus_Store { DateInStore = DateTime.Parse(date1), idArticle = idArticle, Amount = amount };

                // добавляем их в бд
                db.otus_Stories.Add(store1);

                db.SaveChanges();
            }
        }
        public static void ToUser(string name, int age)
        {
            using (otus_UserContext db = new otus_UserContext())
            {
                // создаем объект otus_Store
                otus_User user = new otus_User { Name = name, Age = age};

                // добавляем их в бд
                db.otus_Users.Add(user);

                db.SaveChanges();
            }
        }
        public static double PriceToDate(string date1, Int32 idArticle)
        {
            double price = 0;
            DateTime pdate = DateTime.Parse(date1);
            using (otus_PriceContext db = new otus_PriceContext())
            {
                var prices = db.otus_Prices;
                var lastPrice = (from pr in prices
                                 where pr.idArticle == idArticle && pr.dateIzm <= pdate
                                 select pr).OrderByDescending(pr => pr.dateIzm).Take(1);
                foreach (var l in lastPrice)
                {
                    otus_Price lastPrise = (otus_Price)l;
                    price = lastPrise.PriceArticle;
                }

            }
            return price;
        }
        public static bool IsThereArticle(string date1, Int32 idArticle, Int32 amount)
        {
            double amountFound = 0;
            DateTime pdate = DateTime.Parse(date1);
            using (otus_StoreContext db = new otus_StoreContext())
            {
                var stores = db.otus_Stories;
                amountFound = (from s in stores
                                 where s.idArticle == idArticle && s.DateInStore <= pdate
                                 select s).Sum(s=>s.Amount);
             }
            return amountFound >= amount;
        }
        public static string StoreADay(string date1)
        {
            string resultString = "";
            DateTime pdate = DateTime.Parse(date1);
            using (otus_StoreContext dbs = new otus_StoreContext())
            using (otus_ArticleContext dba = new otus_ArticleContext())
            {
                var stores = dbs.otus_Stories;
                var articles = dba.otus_Articles;
                var list_articles = articles.AsEnumerable();
                var groups = list_articles.Join(stores,
                                       a => a.Id,
                                       s => s.idArticle,
                                       (a, s) => new
                                       { a.NameItem, s.DateInStore, s.Amount }).Where(s => s.DateInStore <= pdate).GroupBy(s => s.NameItem).Select(g => new
                                       {
                                           article = g.Key,
                                           total = g.Sum(x => x.Amount)
                                       });
                foreach (var gr in groups)
                {
                    resultString += gr.article + ": " + gr.total.ToString() + "\r\n";
                }

            }
            return "На " + date1 + " на складе: \r\n" + resultString;
        }
        public static string BestClient()
        {
            string client = "";
            using (otus_ClientContext dbc = new otus_ClientContext())
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                var clients = dbc.otus_Clients;
                var list_clients = clients.AsEnumerable();
                var bestClient = list_clients.Join(orders,
                                       c => c.Id,
                                       o => o.idClient,
                                       (c, o) => new
                                       { fioClient = c.FirstName+" "+c.LastName,
                                         total = o.Amount * Price_Article(o.DateOrder, o.idArticle) })
                                         .Select(c=>c)
                                         .GroupBy(s => s.fioClient)
                                         .Select(g => new
                                       {
                                           fioClient = g.Key,
                                           ttl = g.Sum(x => x.total)
                                       })
                                       .OrderByDescending(x=>x.ttl)
                                       .Take(1);
                foreach (var cl in bestClient)
                {
                    client = cl.fioClient;
                }
            }
            return client;
        }
        public static double Price_Article(DateTime date1, int idArticle)
        {
            double price = 0;
            using (otus_PriceContext db = new otus_PriceContext())
            {
                var prices = db.otus_Prices;
                var lastPrice = (from s in prices
                               where s.idArticle == idArticle && s.dateIzm <= date1
                               select s).OrderByDescending(s=>s.dateIzm).Take(1);
                foreach (var pr in lastPrice)
                {
                    price = pr.PriceArticle;
                }
            }
            return price;
        }
        public static double TotalOrder(Int32 idOrder)
        {
            double total = 0;
            using (otus_OrderContext dbo = new otus_OrderContext())
            {
                var orders = dbo.otus_Orders;
                var list_orders = orders.AsEnumerable();
                total = list_orders.Where(o => o.NOrder == idOrder).Select(o => new { total = o.Amount * Price_Article(o.DateOrder, o.idArticle) }).Sum(o => o.total);
            }
            return total;
        }


    }
}
