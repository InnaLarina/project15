﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DBQueryLibraryEF1
{
    class otus_UserContext: DbContext
    {
        public otus_UserContext() : base("DBConnection")
        { }
        public DbSet<otus_User> otus_Users { get; set; }
    }
}
