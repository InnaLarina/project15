﻿using System.Data.Entity;

namespace DBQueryLibraryEF1
{
    class otus_PriceContext : DbContext
    {
        public otus_PriceContext() : base("DBConnection")
        { }
        public DbSet<otus_Price> otus_Prices { get; set; }
    }
}
