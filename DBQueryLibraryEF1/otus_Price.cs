﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBQueryLibraryEF1
{
    class otus_Price
    {
        public int Id { get; set; }
        public int idArticle { get; set; }
        public DateTime dateIzm { get; set; }
        public double PriceArticle { get; set; }
    }
}
